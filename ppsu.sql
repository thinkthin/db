-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 06, 2018 at 05:21 AM
-- Server version: 5.7.24-log
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nakoding_ppsu`
--

-- --------------------------------------------------------

--
-- Table structure for table `trackhistories`
--

CREATE TABLE `trackhistories` (
  `TrackHistoryID` int(11) NOT NULL,
  `TrackID` int(11) DEFAULT NULL,
  `Latitude` decimal(8,6) DEFAULT NULL,
  `Longitude` decimal(9,6) DEFAULT NULL,
  `Timestamp` timestamp NULL DEFAULT NULL,
  `Status` int(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tracks`
--

CREATE TABLE `tracks` (
  `TrackID` int(11) NOT NULL,
  `UserID` int(11) DEFAULT NULL,
  `TrackName` varchar(50) DEFAULT NULL,
  `Timestamp` timestamp NULL DEFAULT NULL,
  `Status` int(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trackstatuses`
--

CREATE TABLE `trackstatuses` (
  `TrackStatusID` int(11) NOT NULL,
  `TrackID` int(11) NOT NULL DEFAULT '0',
  `TrackStatus` varchar(50) DEFAULT NULL,
  `Timestamp` timestamp NULL DEFAULT NULL,
  `Status` int(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `UserID` int(11) NOT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Username` varchar(30) DEFAULT NULL,
  `Password` varchar(255) DEFAULT NULL,
  `Hash` text,
  `Role` varchar(255) DEFAULT NULL,
  `Timestamp` timestamp NULL DEFAULT NULL,
  `Status` int(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`UserID`, `Name`, `Username`, `Password`, `Hash`, `Role`, `Timestamp`, `Status`) VALUES
(1, 'Admin', 'admin', 'P@ssw0rd', '$2a$10$i6mQodfkXPEwgb6sdqZBauCrk3ifasJ9nEcOR3ewnb0zx6RHnjO9.', 'Admin', '2018-10-03 02:27:52', 1),
(2, 'Worker', 'worker', 'P@ssw0rd', '$2a$10$i4DvLLREwn8L1ADrMnaa7OH96KbjM4di.m0C8dPPt4gK1Z.V.hRQG', 'Worker', '2018-10-23 10:11:31', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `trackhistories`
--
ALTER TABLE `trackhistories`
  ADD PRIMARY KEY (`TrackHistoryID`),
  ADD KEY `TrackID` (`TrackID`);

--
-- Indexes for table `tracks`
--
ALTER TABLE `tracks`
  ADD PRIMARY KEY (`TrackID`),
  ADD KEY `UserID` (`UserID`);

--
-- Indexes for table `trackstatuses`
--
ALTER TABLE `trackstatuses`
  ADD PRIMARY KEY (`TrackStatusID`),
  ADD KEY `TrackID` (`TrackID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`UserID`),
  ADD UNIQUE KEY `Username` (`Username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `trackhistories`
--
ALTER TABLE `trackhistories`
  MODIFY `TrackHistoryID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tracks`
--
ALTER TABLE `tracks`
  MODIFY `TrackID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trackstatuses`
--
ALTER TABLE `trackstatuses`
  MODIFY `TrackStatusID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `UserID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
